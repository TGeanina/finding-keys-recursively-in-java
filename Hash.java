import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Iterator;

public class Hash {
	/**
	 * Function to return all the possible keys of 2 characters
	 * @return keys - all the possible keys
	 */
	private static HashSet<String> findKey()
	{
		//set containing all the possible characters from the keyboard
		char[] set= {'1','2','3','4','5','6','7','8','9','0'};
		
		//the length of the set
		int setLength=set.length;
		
		HashSet<String> keys=new HashSet<String>();
		//finding the keys recursively
		return findKeyRecursively(set,setLength,"",4,keys);
		
	}
	
	/**
	 * Finding the keys recursively
	 * @param set - all the possible characters
	 * @param setLength - the length of the set
	 * @param string - the key
	 * @param s - the size of the key
	 * @param keys - the set containing the possible keys
	 * @return keys - all the possible keys
	 */
	private static HashSet<String> findKeyRecursively(char[] set, int setLength, String string, int s, HashSet<String> keys) {
		
		//base case - finding a key and adding it to the set
		if (s==0)
		{
			keys.add(string);
			return keys;
		}
		
		//combine all the characters from the set one by one
		for(int j=0;j<setLength;++j)
		{
			//adding the next character from the set
			String newKey=string+set[j];
			
			//recursive call (decreased the s because I added a new character)
			findKeyRecursively(set,setLength,newKey,s-1,keys);
		}
		return keys;
	}


	public static void main(String[] args) throws NoSuchAlgorithmException, FileNotFoundException {
	
		HashSet<String> findKey=findKey();
		PrintWriter myWriter=new PrintWriter("output.txt");
		Iterator<String> i = findKey.iterator(); 
        while (i.hasNext()) 
        {
			myWriter.println("SKY-PTNM-" + i.next());
		}
	}
}
